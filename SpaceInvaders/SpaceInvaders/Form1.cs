﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SpaceInvaders.classes;
using System.Threading;

namespace SpaceInvaders
{

    public partial class Form1 : Form
    {
        bool moveLeft;
        bool moveRight;
        bool canShoot;
        int topLim=0;
        int left=-30;
        int i = 2;
        int lives = 1;
        bool getDown = false;
        bool changedDir = false;
        player pl = new player();
        Invaders inv = new Invaders();
        List<PictureBox> invadersList = new List<PictureBox>();
        
        public Form1()
        {
            InitializeComponent();
            startgame();
        }
        public void startgame() {
            new Invaders().CreateSprites(this);
            Invaders.MoveInvToList(this, invadersList);
            pictureBox3.Visible = false;
            label4.Visible = false;
            label3.Visible = false;
            Main.Start();
            DirectCheck.Start();
            Animate.Start();
            laser.Start();

        }
        private void keyIsDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                moveLeft = true;  
            }

            if (e.KeyCode == Keys.Right)
            {
                moveRight = true;
            }
        
    }
        private void keyIsUp(object sender, KeyEventArgs e)
        {
            {
                if (e.KeyCode == Keys.Left)
                {

                    moveLeft = false;
                }

                if (e.KeyCode == Keys.Right)
                {

                    moveRight = false;
                }
                else if (e.KeyCode == Keys.Space && canShoot)
                {
                    rocket.RocketAddControl(this, player);
                    canShoot = false;
                }
            }
        }
      
        public bool isLevelClear() 
        {
            int count=0;
            foreach (Control inv in Controls)
                if (inv is PictureBox && inv.Name == "Invader1"|| inv.Name == "Invader2"|| inv.Name == "Invader3") count++;
            if (count == 0) return true;
            else return false;
        }
        public void Beam(PictureBox a)
        {
            PictureBox invaderLaser = new PictureBox();
            invaderLaser.Location = new Point(a.Location.X + a.Width / 3, a.Location.Y + 20);
            invaderLaser.Size = new Size(5, 20);
            invaderLaser.BackgroundImage = Properties.Resources.Bullet;
            invaderLaser.BackgroundImageLayout = ImageLayout.Stretch;
            invaderLaser.Name = "Laser";
            Controls.Add(invaderLaser);
        }
        private void StrikeSpan(object sender, EventArgs e)
        {
            Random r = new Random();
            if (!isLevelClear())
            {   
                  Beam(invadersList[r.Next(0, invadersList.Count)]);
            }
        }
       
        public bool Touched(PictureBox a, int top)
        {
            return a.Location.X <= 0 || a.Location.X >= top;
        }

        public void Collided(Control invader)
        {
            if (invader.Bounds.IntersectsWith(player.Bounds))
            {
                gameover();
            }
        }
        private void BulletDelay_Tick(object sender, EventArgs e)
        {
            canShoot = true;
        }

        
        private void Animate_Tick(object sender, EventArgs e)
        {
            i++;
            Invaders.InvBehavior(this,i, left, topLim);
            
        }

        private void InvaderAI_Tick(object sender, EventArgs e)
        {
          
        }
        public void gameover() 
        {
            Main.Stop();
            DirectCheck.Stop();
            Animate.Stop();
            laser.Stop();
            label4.Visible = true;
            label4.Text = pl.getPlayerScoreString();
            label5.Visible = true;
            pictureBox3.Visible = true;
        }
        private void DirectAndUD_Tick(object sender, EventArgs e)
        {
            
            if (!isLevelClear())
            {
               
                if (((invadersList.ElementAt(0).Left < 40)) || (invadersList.ElementAt(invadersList.Count - 1).Left > 800))
                {
                    if (!changedDir)
                    {
                        left = left * -1;
                        changedDir = true;
                    }
                    if (!getDown) 
                    {
                        foreach (PictureBox a in invadersList)
                        {
                            a.Top += 50;
                            getDown = true;
                        }
                    }
                    
                }
                if (invadersList.ElementAt(0).Left < 200 && invadersList.ElementAt(0).Left > 120) { getDown = false; changedDir = false; }
            }
        }
        public void LoseLife()
        {
            if (lives > -1)
            {
                player.Left = 350;
                lives -= 1;
                if (pictureBox1.Visible == true) { pictureBox1.Visible = false; }
                else pictureBox2.Visible = false;
            }
            else
            {
                gameover();
            }
        }
        public void LaserBehavior()
        {
            foreach (Control c in Controls)
            {
                if (c.Name == "Laser")
                {
                    PictureBox laser = (PictureBox)c;
                    laser.Top += 5;

                    if (laser.Location.Y >= 560)
                    {
                        Controls.Remove(laser);
                    }
                    if (laser.Bounds.IntersectsWith(player.Bounds))
                    {
                        Controls.Remove(laser);
                        LoseLife();
                    }
                    foreach (Control c1 in Controls)
                    {

                        if (c1.Name == "rocket")
                        {
                            if (laser.Bounds.IntersectsWith(c1.Bounds))
                            {
                                Controls.Remove(laser);
                                Controls.Remove(c1);
                                pl.IncraseScoreL();
                            }
                        }
                    }
                }
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            LaserBehavior();//
            if (moveLeft && player.Left > 0)//
            {
                pl.moveLeft(player);//
            }
            else if (moveRight && player.Left < 815)//
            {
                pl.moveRight(player);//
            }
            rocket.RocketFly(this, invadersList, pl);
            if (isLevelClear())
            {
                winScene();
            }
            else
            {
                label1.Text = pl.getPlayerScoreString();
            }

           
            foreach (Control a in Controls) 
            {
                if (a is PictureBox && a.Name == "Invader1" || a.Name == "Invader2" || a.Name == "Invader3") 
                {
                    Collided(a);
                }
            }
        }
        public void winScene() {
            Main.Stop();
            DirectCheck.Stop();
            Animate.Stop();
            laser.Stop();
            label4.Visible = true;
            label3.Visible = true;
            pictureBox3.Visible = true;
            label4.Text=pl.getPlayerScoreString();
        }

        private void laser_Tick(object sender, EventArgs e)
        {
            StrikeSpan(sender, e);
        }
    }
}
