﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpaceInvaders
{
    class player : objects
    {
        private int score;
        
        public player()
        {
            speed = 6;
            score = 0;
        }
        public int getPlayerSpeed()
        {
            return speed;
        }
        public string getPlayerScoreString()
        {
            return "Score: " + Convert.ToString(score);
        }
        public void IncraseScore()
        {
            Random re = new Random();
            int ig = re.Next(3, 7);
            score += ig;
        }
        public void IncraseScoreL()
        {
            score += 3;
        }
        public void moveRight(PictureBox pl)
        {
            pl.Left += getPlayerSpeed();
        }
        public void moveLeft(PictureBox pl)
        {
            pl.Left -= getPlayerSpeed();
        }
    }
}
