﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SpaceInvaders.classes
{
    class Invaders : objects
    {
        int x_for_Rest;
        public Invaders()
        {
            width = 40;
            height = 30;
            x_for_Rest = 450;
            x = x_for_Rest;
            y = 0;

        }
        private void CreateInvader(Form p, int i)//
        {
            PictureBox invader = new PictureBox();
            invader.Size = new Size(width, height);
            invader.Location = new Point(x, y);
            if (i == 1)
            {
                invader.BackgroundImage = Properties.Resources.InvaderA1;
                invader.Name = "Invader1";
            }
            else if (i == 2)
            {
                invader.BackgroundImage = Properties.Resources.InvaderB1;
                invader.Name = "Invader2";
            }
            else
            {
                invader.BackgroundImage = Properties.Resources.InvaderC1;
                invader.Name = "Invader3";
            }

            invader.BackgroundImageLayout = ImageLayout.Stretch;

            p.Controls.Add(invader);
        }

        public void CreateSprites(Form p)//
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    CreateInvader(p, i);
                    x += width + 40;
                }
                y += height + 40;
                x = x_for_Rest;
            }
        }
        static public void MoveInvToList(Form p, List<PictureBox> a)//
        {
            foreach (Control c in p.Controls)
            {
                if (c is PictureBox && (c.Name == ("Invader1")) || (c.Name == ("Invader2")) || (c.Name == ("Invader3")))
                {
                    PictureBox inv = (PictureBox)c;
                    a.Add(inv);
                }
            }
        }
        static public void InvBehavior(Form frm, int i, int left, int topLim)//
        {
            foreach (Control ctrl in frm.Controls)
            {
                if (ctrl is PictureBox && (ctrl.Name == "Invader1" || ctrl.Name == "Invader2" || ctrl.Name == "Invader3"))
                {
                    ctrl.Location = new Point(ctrl.Location.X + left, ctrl.Location.Y + topLim);
                }
                if (ctrl is PictureBox && ctrl.Name == "Invader1")
                {
                    if (i % 2 == 0) { ctrl.BackgroundImage = Properties.Resources.InvaderA2; }
                    else
                        ctrl.BackgroundImage = Properties.Resources.InvaderA1;

                }
                else
                if (ctrl is PictureBox && ctrl.Name == "Invader2")
                {
                    if (i % 2 == 0) { ctrl.BackgroundImage = Properties.Resources.InvaderB2; }
                    else
                        ctrl.BackgroundImage = Properties.Resources.InvaderB1;

                }
                else
                if (ctrl is PictureBox && ctrl.Name == "Invader3")
                {
                    if (i % 2 == 0) { ctrl.BackgroundImage = Properties.Resources.InvaderC2; }
                    else
                        ctrl.BackgroundImage = Properties.Resources.InvaderC1;
                }
            }
        }        
    }
}
