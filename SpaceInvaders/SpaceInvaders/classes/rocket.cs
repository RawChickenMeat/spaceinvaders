﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SpaceInvaders.classes;
namespace SpaceInvaders
{
    class rocket : objects
    {
        public rocket()
            {
            speed=15;
            }
           static public void RocketAddControl(Form w, PictureBox i) {
            PictureBox rocket = new PictureBox();
            rocket.Location = new Point(i.Location.X + 27, i.Location.Y - 30);
            rocket.Size = new Size(5, 20);
            rocket.BackgroundImage = Properties.Resources.Bullet;
            rocket.BackgroundImageLayout = ImageLayout.Stretch;
            rocket.Name = "rocket";
            w.Controls.Add(rocket);
        }
       static public void RocketFly(Form frm, List <PictureBox> aliens,player pl)
        {
            foreach (Control c in frm.Controls)
            {

                    if (c is PictureBox && c.Name == "rocket")
                    {
                    PictureBox rocket1 = (PictureBox)c;
                    rocket1.Top -= GetRocketSpeed();

                    if (rocket1.Location.Y < 0)
                    {
                        frm.Controls.Remove(rocket1);
                    }
                    
                    foreach (Control ct in frm.Controls)
                    {
                        
                        if (ct is PictureBox && ((ct.Name == "Invader1") || (ct.Name == "Invader2") || (ct.Name == "Invader3")))
                        {
                            PictureBox alien = (PictureBox)ct;
                                
                            if (rocket1.Bounds.IntersectsWith(alien.Bounds))
                            {
                                frm.Controls.Remove(rocket1);
                                frm.Controls.Remove(alien);
                                aliens.Remove(alien);
                                pl.IncraseScore();
                            }

                        }
                    }
                }
            }
        }



        
       static public int GetRocketSpeed() {
            rocket i = new rocket();
            return i.speed;
        }
    } 

}